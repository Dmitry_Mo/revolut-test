## Build
insert free port for unt test to bind(optional, default 8080):

mvn clean package -Dport=<port>

## Run
insert port on which the server will run(required):

java -jar MoneyBackend-1.0-SNAPSHOT-jar-with-dependencies.jar -p <port>

## API

GET  /account/{id}

GET  /account/all

POST  /account/                      {"name":"<name>", "balance":<initialBalance>}

POST /transfer/                      {"idTo":"<id1>", "idFrom":"<id2>", "amount":<amount of money>}


