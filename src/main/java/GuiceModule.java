import com.google.inject.AbstractModule;
import core.IAccountDataStore;
import core.ITransferService;
import core.impl.AccountDataStoreImpl;
import core.impl.TransferServiceImpl;

public class GuiceModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(ITransferService.class).to(TransferServiceImpl.class);
        bind(IAccountDataStore.class).to(AccountDataStoreImpl.class);
        requestInjection(Service.class);
    }
}

