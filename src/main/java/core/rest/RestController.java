package core.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import core.Account;
import core.IAccountDataStore;
import core.ITransferService.ValidationException;
import core.impl.TransferServiceImpl;
import core.requests.AccountCreationRequest;
import core.requests.TransferRequest;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;

import java.io.IOException;
import java.util.List;
import java.util.StringJoiner;

public class RestController {

    private IAccountDataStore accountDataStore;
    private TransferServiceImpl transferService;

    @Inject
    public void setAccountDataStore(IAccountDataStore accountDataStore) {
        this.accountDataStore = accountDataStore;
    }

    @Inject
    public void setTransferService(TransferServiceImpl transferService) {
        this.transferService = transferService;
    }

    public void createAccount(HttpServerExchange exchange) {
        exchange.getRequestReceiver().receiveFullString(((httpServerExchange, s) -> {
            ObjectMapper mapper = new ObjectMapper();
            try {
                AccountCreationRequest request = mapper.readValue(s, AccountCreationRequest.class);
                Long accId = accountDataStore.addAccount(request.getName(), request.getBalance());
                exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
                exchange.getResponseHeaders().put(Headers.STATUS, 200);
                exchange.getResponseSender().send("{\"id\":" + accId + "}");

            } catch (IOException e) {
                exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/html");
                exchange.getResponseHeaders().put(Headers.STATUS, 400);
                exchange.getResponseSender().send("input json is not correct");
            }

        }));
    }

    public void getAccount(HttpServerExchange exchange) {
        Account account = accountDataStore.getAccount(Long.valueOf(exchange.getQueryParameters().get("accountId").getFirst()));
        if (account != null) {
            exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
            exchange.getResponseHeaders().put(Headers.STATUS, 200);
            exchange.getResponseSender().send(account.toJson());
        } else {
            exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/html");
            exchange.getResponseHeaders().put(Headers.STATUS, 400);
            exchange.getResponseSender().send("account does not exist");
        }
    }

    public void getAllAccounts(HttpServerExchange exchange) {
        List<Account> accounts = accountDataStore.getAccounts();
        if (accounts != null) {
            exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
            exchange.getResponseHeaders().put(Headers.STATUS, 200);

            StringBuilder resJson = new StringBuilder();
            resJson.append("[");
            StringJoiner joiner = new StringJoiner(",");
            for (Account a : accounts) {
                joiner.add(a.toJson());
            }
            resJson.append(joiner.toString());
            resJson.append("]");

            exchange.getResponseSender().send(resJson.toString());
        } else {
            exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/html");
            exchange.getResponseHeaders().put(Headers.STATUS, 400);
            exchange.getResponseSender().send("account does not exist");
        }

    }

    public void transferMoney(HttpServerExchange exchange) {
        exchange.getRequestReceiver().receiveFullString(((httpServerExchange, s) -> {
            ObjectMapper mapper = new ObjectMapper();
            try {
                TransferRequest request = mapper.readValue(s, TransferRequest.class);
                transferService.transfer(request.getIdFrom(), request.getIdTo(), request.getAmount());
                exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/html");
                exchange.getResponseHeaders().put(Headers.STATUS, 200);
                exchange.getResponseSender().send("transfer done");
            } catch (IOException e) {
                exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/html");
                exchange.getResponseHeaders().put(Headers.STATUS, 400);
                exchange.getResponseSender().send("input json is not correct");
            } catch (ValidationException e) {
                exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "text/html");
                exchange.getResponseHeaders().put(Headers.STATUS, 400);
                exchange.getResponseSender().send(e.getLocalizedMessage());
            }
        }));
    }


}
