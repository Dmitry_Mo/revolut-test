package core.requests;

import java.math.BigDecimal;

public class TransferRequest {
    private Long idFrom;
    private Long idTo;
    private BigDecimal amount;

    public TransferRequest(Long idFrom, Long idTo, BigDecimal amount) {
        this.idFrom = idFrom;
        this.idTo = idTo;
        this.amount = amount;
    }

    public TransferRequest() {
    }

    public Long getIdFrom() {
        return idFrom;
    }

    public void setIdFrom(Long idFrom) {
        this.idFrom = idFrom;
    }

    public long getIdTo() {
        return idTo;
    }

    public void setIdTo(Long idTo) {
        this.idTo = idTo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
