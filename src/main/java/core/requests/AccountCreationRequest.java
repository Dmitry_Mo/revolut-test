package core.requests;

import java.math.BigDecimal;

public class AccountCreationRequest {
    private String name;
    private BigDecimal balance;

    public AccountCreationRequest(String name, BigDecimal ballance) {
        this.name = name;
        this.balance = ballance;
    }

    public AccountCreationRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
