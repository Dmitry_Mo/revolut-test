package core;

import java.math.BigDecimal;

public interface ITransferService {

    void transfer(Long from, Long to, BigDecimal amount);

    class ValidationException extends RuntimeException {
        public ValidationException(String message) {
            super(message);
        }
    }
}

