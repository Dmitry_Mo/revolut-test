package core.impl;

import com.google.inject.Singleton;
import core.Account;
import core.IAccountDataStore;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

@Singleton
public class AccountDataStoreImpl implements IAccountDataStore {
    private ConcurrentHashMap<Long, Account> dataStrore;
    private AtomicLong idCounter;

    public AccountDataStoreImpl() {
        this.dataStrore = new ConcurrentHashMap<>();
        this.idCounter = new AtomicLong(0);
    }

    @Override
    public Long addAccount(String name, BigDecimal initialBalance) {
        Long id = idCounter.incrementAndGet();
        Account newAccount = new Account(id, name, initialBalance);
        dataStrore.put(id, newAccount);
        return id;
    }

    @Override
    public Account getAccount(Long id) {
        return dataStrore.get(id);
    }

    @Override
    public List<Account> getAccounts() {
        List<Account> res = new ArrayList<>();
        dataStrore.values().stream().sorted(Comparator.comparing(Account::getId)).forEach(e -> {
            res.add(e.copy());
        });
        return res;

    }
}
