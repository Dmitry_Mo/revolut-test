package core.impl;


import com.google.inject.Inject;
import core.Account;
import core.IAccountDataStore;
import core.ITransferService;

import java.math.BigDecimal;

public class TransferServiceImpl implements ITransferService {
    private IAccountDataStore accountDataStore;

    @Inject
    public void setAccountDataStore(IAccountDataStore accountDataStore) {
        this.accountDataStore = accountDataStore;
    }

    //this function avoids deadlocks by lock order - smaller Ids are locked first
    @Override
    public void transfer(Long from, Long to, BigDecimal amount) {
        if (from > to) {
            Account targetAccount = accountDataStore.getAccount(to);
            if (targetAccount == null) {
                throw new ValidationException("target account does not exist");
            }
            synchronized (targetAccount) {
                Account sourceAccount = accountDataStore.getAccount(from);
                if (sourceAccount == null) {
                    throw new ValidationException("source account does not exist");
                }
                synchronized (sourceAccount) {
                    if (sourceAccount.getBalance().compareTo(amount) <= -1) {
                        throw new ValidationException("can't withdraw - not enough money");
                    }
                    sourceAccount.setBalance(sourceAccount.getBalance().subtract(amount));
                    targetAccount.setBalance(targetAccount.getBalance().add(amount));
                }
            }
        } else {
            Account sourceAccount = accountDataStore.getAccount(from);
            if (sourceAccount == null) {
                throw new ValidationException("source account does not exist");
            }
            synchronized (sourceAccount) {
                Account targetAccount = accountDataStore.getAccount(to);
                if (targetAccount == null) {
                    throw new ValidationException("target account does not exists");
                }
                synchronized (targetAccount) {
                    if (sourceAccount.getBalance().compareTo(amount) <= -1) {
                        throw new ValidationException("can't withdraw - not enough money");
                    }
                    sourceAccount.setBalance(sourceAccount.getBalance().subtract(amount));
                    targetAccount.setBalance(targetAccount.getBalance().add(amount));
                }
            }
        }
    }


}
