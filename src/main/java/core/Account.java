package core;

import java.math.BigDecimal;
import java.util.Objects;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Account {
    private Logger logger = LoggerFactory.getLogger(Account.class);

    private Long id;
    private BigDecimal balance;
    private String name;

    public Account(Long id,String name, BigDecimal balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public Account() {
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        //this is safe because BigDecimal is immutable. Should have been copy instead of assignment otherwise
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public String toJson() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            logger.error(e.toString());
        }
        return "unable to convert to json";
    }

    public Account copy() {
        return new Account(this.id,this.name, this.balance);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) &&
                Objects.equals(balance, account.balance) &&
                Objects.equals(name, account.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, balance, name);
    }
}
