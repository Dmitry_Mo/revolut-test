package core;

import java.math.BigDecimal;
import java.util.List;

public interface IAccountDataStore {
    Long addAccount(String name, BigDecimal initialBalance);

    Account getAccount(Long id);

    List<Account> getAccounts();
}
