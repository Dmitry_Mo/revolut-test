import com.google.inject.Inject;
import core.rest.RestController;
import io.undertow.Handlers;
import io.undertow.Undertow;


public class Service {
    @Inject
    private RestController restController;

    public void start(Integer port) {
        Undertow server = Undertow.builder()
                .addHttpListener(port, "localhost")
                .setHandler(Handlers.exceptionHandler(Handlers.path()
                        .addPrefixPath("/account", Handlers.routing()
                                .post("", restController::createAccount)
                                .get("/{accountId}", restController::getAccount)
                                .get("/all", restController::getAllAccounts)
                        )
                        .addPrefixPath("/transfer", Handlers.routing()
                                .post("", restController::transferMoney)
                        ))
                ).build();
        server.start();
    }

    public RestController getRestController() {
        return restController;
    }

    public void setRestController(RestController restController) {
        this.restController = restController;
    }
}
