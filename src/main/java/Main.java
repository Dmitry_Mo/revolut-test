import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);

    //accepts port in params
    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new GuiceModule());
        Service service = injector.getInstance(Service.class);

        CommandLine commandLine = null;
        Option portOption = Option.builder("p")
                .required(true)
                .longOpt("port")
                .hasArg(true)
                .desc("server port")
                .build();
        Options options = new Options();
        options.addOption(portOption);
        CommandLineParser parser = new DefaultParser();
        try {
            commandLine = parser.parse(options, args);
        } catch (ParseException exception) {
            logger.error(exception.getLocalizedMessage());
        }
        Integer port = Integer.valueOf(commandLine.getOptionValue("p"));
        service.start(port);
    }
}
