import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.Account;
import core.impl.AccountDataStoreImpl;
import core.impl.TransferServiceImpl;
import core.requests.AccountCreationRequest;
import core.requests.TransferRequest;
import core.rest.RestController;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertEquals;

public class RestServerTest {

    private Integer port;

    @Before
    public void before() {

        String stringPOrt = System.getProperties().getProperty("port");
        if (stringPOrt == null) {
            this.port = 8088;
        } else {
            this.port = Integer.valueOf(stringPOrt);
        }
        Service service = new Service();

        RestController restController = new RestController();
        AccountDataStoreImpl accountDataStore = new AccountDataStoreImpl();
        TransferServiceImpl transferService = new TransferServiceImpl();
        transferService.setAccountDataStore(accountDataStore);
        restController.setAccountDataStore(accountDataStore);
        restController.setTransferService(transferService);

        service.setRestController(restController);
        service.start(port);
    }

    @Test
    public void test() throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        //create account 1
        createAccount(1,httpClient, "test1");
        createAccount(2, httpClient, "test2");
        transfer(httpClient, 1L, 2L, "1");
        Account ac1 = fetchAccount(httpClient, 1L);
        Account ac2 = fetchAccount(httpClient, 2L);

        assertEquals(new BigDecimal(101), ac1.getBalance());
        assertEquals(new BigDecimal(99), ac2.getBalance());

        List<Account> accounts = fetchAccounts(httpClient).stream().sorted(Comparator.comparing(Account::getId)).collect(Collectors.toList());
        assertEquals(accounts.get(0), ac1);
        assertEquals(accounts.get(1), ac2);


    }

    private Account fetchAccount(CloseableHttpClient httpClient, Long id) throws Exception {
        HttpGet get = new HttpGet("http://localhost:" + port + "/account/" + id);
        CloseableHttpResponse httpResponse = httpClient.execute(get);
        String response = EntityUtils.toString(httpResponse.getEntity());

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(response, Account.class);
    }

    private List<Account> fetchAccounts(CloseableHttpClient httpClient) throws Exception {
        HttpGet get = new HttpGet("http://localhost:" + port + "/account/all");
        CloseableHttpResponse httpResponse = httpClient.execute(get);
        String response = EntityUtils.toString(httpResponse.getEntity());

        ObjectMapper mapper = new ObjectMapper();
        List<Account> accounts = mapper.readValue(response, new TypeReference<List<Account>>(){});
        return accounts;
    }

    //accNum is used to simplify id evaluation
    private void createAccount(int accNum,CloseableHttpClient httpClient, String name) throws IOException {
        HttpPost post = new HttpPost("http://localhost:" + port + "/account/");
        post.addHeader("Content-Type", "application/json");
        AccountCreationRequest accountCreationRequest = new AccountCreationRequest(name, new BigDecimal(100));

        ObjectMapper mapper = new ObjectMapper();
        String req = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(accountCreationRequest);
        post.setEntity(new StringEntity(req));

        CloseableHttpResponse httpResponse = httpClient.execute(post);
        String response = EntityUtils.toString(httpResponse.getEntity());
        assertEquals("{\"id\":"+accNum+"}", response);
    }

    private void transfer(CloseableHttpClient httpClient, Long idTo, Long idFrom, String amount) throws IOException {
        HttpPost post = new HttpPost("http://localhost:" + port + "/transfer/");
        post.addHeader("Content-Type", "application/json");
        TransferRequest transferRequest = new TransferRequest(idFrom, idTo, new BigDecimal(amount));

        ObjectMapper mapper = new ObjectMapper();
        String req = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(transferRequest);
        post.setEntity(new StringEntity(req));

        CloseableHttpResponse httpResponse = httpClient.execute(post);
        String response = EntityUtils.toString(httpResponse.getEntity());
        assertEquals("transfer done", response);
    }

}
