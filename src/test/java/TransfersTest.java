import core.ITransferService;
import core.impl.AccountDataStoreImpl;
import core.impl.TransferServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransfersTest {
    private AccountDataStoreImpl accountDataStore;
    private TransferServiceImpl transferService;

    @Before
    public void before() {
        accountDataStore = new AccountDataStoreImpl();
        transferService = new TransferServiceImpl();
        transferService.setAccountDataStore(accountDataStore);
    }

    @Test
    public void test() {
        accountDataStore.addAccount("first", new BigDecimal(100));
        accountDataStore.addAccount("second", new BigDecimal(100));

        transferService.transfer(1L, 2L, new BigDecimal(1));

        assertEquals(new BigDecimal(99), accountDataStore.getAccount(1L).getBalance());
        assertEquals(new BigDecimal(101), accountDataStore.getAccount(2L).getBalance());

        transferService.transfer(2L, 1L, new BigDecimal(2));

        assertEquals(new BigDecimal(99), accountDataStore.getAccount(2L).getBalance());
        assertEquals(new BigDecimal(101), accountDataStore.getAccount(1L).getBalance());
    }

    @Test(expected = ITransferService.ValidationException.class)
    public void testNoAccount() {
        accountDataStore.addAccount("first", new BigDecimal(100));
        transferService.transfer(1L, 2L, new BigDecimal(1));

    }
}


